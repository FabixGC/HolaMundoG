package gc.holamundogc.vista;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import gc.holamundogc.R;
import gc.holamundogc.sqlite.HolaMundoDBContract;
import gc.holamundogc.sqlite.UsuariosModel;


public class FormularioActivity extends AppCompatActivity {

    private EditText etUsername, etPassword1, etPassword2;
    private Button btRegister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        this.etUsername = (EditText) findViewById(R.id.etUsername);
        this.etPassword1 = (EditText) findViewById(R.id.etPassword1);
        this.etPassword2 = (EditText) findViewById(R.id.etPassword2);

        this.btRegister = (Button) findViewById(R.id.btRegister);

        this.btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Llamar UsuarioModel para agregar un usuario a la base de datos
                ContentValues usuario = new ContentValues();
                usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME, etUsername.getText().toString());
                usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD, etPassword1.getText().toString());

                UsuariosModel usuariosModel = new UsuariosModel(getApplicationContext());
                usuariosModel.crearUsuario(usuario);
            }
        });

    }
}



















