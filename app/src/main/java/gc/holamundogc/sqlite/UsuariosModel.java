package gc.holamundogc.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;



public class UsuariosModel {
    private HolaMundoDBHelper dbHelper;

    public UsuariosModel(Context context){
        this.dbHelper = new HolaMundoDBHelper(context);
    }

    public void crearUsuario(ContentValues usuario){
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(HolaMundoDBContract.HolaMundoUsuarios.TABLE_NAME, null, usuario);
    }
}
