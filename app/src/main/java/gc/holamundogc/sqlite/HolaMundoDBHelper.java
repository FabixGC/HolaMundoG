package gc.holamundogc.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public class HolaMundoDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "HolaMundoGC.db";
    public static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE =
            "CREATE TABLE " + HolaMundoDBContract.HolaMundoUsuarios.TABLE_NAME +
                    "(" + HolaMundoDBContract.HolaMundoUsuarios._ID + " INTEGER PRIMARY KEY," +
                    HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME + " TEXT," +
                    HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD + " TEXT)";

    public HolaMundoDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
